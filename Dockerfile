FROM php:5-apache
MAINTAINER Michaël Rivet <rivet.michael@gmail.com>

# Installs php extensions
RUN apt-get update && apt-get install -y \
      libfreetype6-dev \
      libjpeg62-turbo-dev \
      libmcrypt-dev \
      libpng12-dev \
      mysql-client \
      ssmtp
RUN docker-php-ext-install mcrypt gd pdo_mysql mbstring \
    && pecl install xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer

# Custom PHP ini configuration
COPY ./z-php.ini /usr/local/etc/php/conf.d/

# Default document root
ENV APACHE_DOC_ROOT=/srv/
# Silent composer message
ENV COMPOSER_DISABLE_XDEBUG_WARN=1

# Activating mod rewrite
RUN a2enmod rewrite

# Add www-data to root group and viceversa
RUN usermod -u 1000 www-data

# Default index.php for testing
COPY public/index.php /srv/

# Volume for Apache root directory
VOLUME /srv
WORKDIR /srv

# Start!
COPY ./run.sh /
RUN chmod +x /run.sh
CMD ["/run.sh"]
