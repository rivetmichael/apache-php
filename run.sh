#!/usr/bin/env bash
set -e

# Changing to custom document root
sed -i "s|DocumentRoot /var/www/html|DocumentRoot $APACHE_DOC_ROOT|" /etc/apache2/apache2.conf
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf

# Writing custom configuration
mkdir -p $APACHE_DOC_ROOT
echo "<Directory $APACHE_DOC_ROOT>" > /etc/apache2/conf-available/document-root-directory.conf
echo "	AllowOverride All" >> /etc/apache2/conf-available/document-root-directory.conf
echo "	Require all granted" >> /etc/apache2/conf-available/document-root-directory.conf
echo "</Directory>" >> /etc/apache2/conf-available/document-root-directory.conf

# Enabling the document-root-directory file
a2enconf "document-root-directory.conf"

exec "apache2-foreground"
